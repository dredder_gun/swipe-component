(ns swipe.css
  (:require [garden.def :refer [defstyles
                                defkeyframes]]))

(def main-wrapper
  [:.main-wrapper {:width "100%"
                   :height "100%"
                   :overflow "hidden"
                   ;:-webkit-transition "left"
                   ;:transition "left"
                   }])

(def shots
  [:.shot {:position "relative"
           :height "500px"
           :margin 0
           :display "inline-block"}])

(defstyles swipe-styles
           main-wrapper
           shots)