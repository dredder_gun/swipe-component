(ns swipe.events
  (:require [re-frame.core :as re-frame]
            [swipe.db :as db]))

(re-frame/reg-event-db
 ::initialize-db
 (fn  [_ _]
   db/default-db))

(def add-swipe-component-key
  "Чтобы в другом проекте app-db не смешивался, кладём его в keyword :swipe-component"
  (re-frame/->interceptor
    :id      :add-swipe-component-key
    :before (fn [context]
              (let [swipe-component-db (get-in context [:coeffects :db :swipe-component])]
                (-> (update-in context [:coeffects :db] dissoc :swipe-component)
                    (update-in [:coeffects :db] merge swipe-component-db))))
    :after  (fn [context]
              (let [db (get-in context [:effects :db])]
                (-> (update-in context [:effects :db] dissoc :start-touch-position
                                                             :main-wrapper-width
                                                             :current-slides
                                                             :right-next-slide
                                                             :left-next-slide
                                                             :temporary-swipe
                                                             :slides-count
                                                             :focus-slide
                                                             :start-swipe-time
                                                             :direction
                                                             :sw-mod
                                                             :duration
                                                             :fast-swipe?)
                    (assoc-in [:effects :db :swipe-component] {:start-touch-position (:start-touch-position db)
                                                               :main-wrapper-width (:main-wrapper-width db)
                                                               :current-slides (:current-slides db)
                                                               :right-next-slide (:right-next-slide db)
                                                               :left-next-slide (:left-next-slide db)
                                                               :temporary-swipe (:temporary-swipe db)
                                                               :slides-count (:slides-count db)
                                                               :focus-slide (:focus-slide db)
                                                               :start-swipe-time (:start-swipe-time db)
                                                               :direction (:direction db)
                                                               :sw-mod (:sw-mod db)
                                                               :duration (:duration db)
                                                               :fast-swipe? (:fast-swipe? db)}))))))

(re-frame/reg-event-db
  ::setup
  [add-swipe-component-key re-frame/trim-v]
  (fn [db [slides sw-mod & args]]
    (let [carousel? (= :carousel sw-mod)
          width (first args)]
      (->
        (cond
          ; поведение с одним слайдом для всех режимов одинаковое
          (= 1 (count slides)) (assoc db :slides-count 1
                                         :current-slides [0])
          ; карусель с двумя слайдами
          (and carousel? (= 2 (count slides))) (assoc db :slides-count 2
                                                         :current-slides [1 0 1])
          ; полоска с двумя слайдами
          (= 2 (count slides)) (assoc db :slides-count 2
                                         :current-slides [0 1])
          ; карусель > 2 слайдов
          carousel? (assoc db :current-slides [(- (count slides) 1) 0 1])
          ; полоска > 2 слайдов
          :default (assoc db :current-slides [0 1 2]))
        ; общиен настройки
        (assoc :focus-slide 0
               :sw-mod sw-mod
               :slides-count (count slides))
        ; если задана ширина, то занесём её в app-db
        (#(if (nil? width)
              %
              (assoc % :main-wrapper-width width)))))))

(re-frame/reg-event-db
  ::start-coord
  [add-swipe-component-key re-frame/trim-v]
  (fn [db [coord]]
    (assoc db :start-touch-position coord
              :start-swipe-time (.getTime (js/Date.)))))

(re-frame/reg-event-db
  ::swipe-element
  [add-swipe-component-key re-frame/trim-v]
  (fn [db [position]]
    (let [delta (- (:start-touch-position db) position)
          direction (/ (Math/abs delta) delta)
          first-slide? (= (:focus-slide db) 0)
          last-slide? (= (:focus-slide db) (- (:slides-count db) 1))
          slow-swipe (+ (/ (Math/abs delta) (:main-wrapper-width db)) 1)
          not-carousel? (not= (:sw-mod db) :carousel)
          left? (= direction -1)
          right? (= direction 1)]
      (-> (if (> (Math/abs delta) (/ (:main-wrapper-width db) 2)) ; свайпнули ли до середины слайда?
            (cond
              ; если слайд с краю и при этом режим :line
              (and not-carousel? (or (and last-slide?
                                          right?)
                                     (and first-slide?
                                          left?))) (assoc db :temporary-swipe slow-swipe)
              ; вправо
              right? (-> (assoc db :right-next-slide true)
                                  (assoc :temporary-swipe (* -1 delta)))
              ; влево
              left? (-> (assoc db :left-next-slide true)
                                   (assoc :temporary-swipe (* -1 delta)))
              :default (do (.error js/console "Неизвестное состояние слайдера")
                           db))
            (if (and (or (and left?
                              first-slide?)
                         (and right?
                              last-slide?))
                     (not= :carousel (:sw-mod db)))
              (assoc db :temporary-swipe slow-swipe)
              (assoc db :temporary-swipe (* -1 delta))))
          (assoc :direction direction)))))

(re-frame/reg-event-db
  ::end-move
  [add-swipe-component-key]
  (fn [db _]
    (let [fast-swipe? (< (- (.getTime (js/Date.)) (:start-swipe-time db)) 250)]
      (-> (cond
            ; когда один кадр и пытаются пролистнуть
            (and (or (:right-next-slide db)
                     (:left-next-slide db)
                     fast-swipe?)
                 (= (:slides-count db) 1)) db
            ; два кадра, режим :carousel
            (and (= (:sw-mod db) :carousel)
                 (= (:slides-count db) 2)) (condp = (:focus-slide db)
                                                         1 (-> (assoc db :focus-slide 0)
                                                               (assoc :current-slides [0 1 0]))
                                                         0 (-> (assoc db :focus-slide 1)
                                                               (assoc :current-slides [1 0 1]))
                                                         (do
                                                           (.error js/console ":focus-slide может быть равен только 0 и 1")
                                                           (assoc db :current-slides [1 0 1])))
            ; два кадра, пролистывают вправо, режим :line
            (and (or (:right-next-slide db)
                     (and (= 1 (:direction db))
                          fast-swipe?))
                 (= (:slides-count db) 2)) (condp = (:focus-slide db)
                                                     1 db
                                                     0 (assoc db :focus-slide 1)
                                                     (do
                                                       (.error js/console "Когда 2 кадра, :focus-slide
                                                                           может быть равен только 0 и 1")
                                                       (assoc db :focus-slide 0)))
            ; два кадра, пролистывают влево, режим :line
            (and (or (:left-next-slide db)
                     (and (= -1 (:direction db))
                          fast-swipe?))
                 (= (:slides-count db) 2)) (condp = (:focus-slide db)
                                                     1 (assoc db :focus-slide 0)
                                                     0 db
                                                     (do
                                                       (.error js/console "Когда 2 кадра, :focus-slide
                                                                           может быть равен только 0 и 1")
                                                       (assoc db :focus-slide 0)))
            ; пролистали вправо в режиме :carousel
            (and (= (:sw-mod db) :carousel)
                 (or (:right-next-slide db)
                     (and (= 1 (:direction db))
                          fast-swipe?))) (condp = (- (:slides-count db) (:focus-slide db))
                                            2 (assoc db :current-slides [(- (:slides-count db) 2) (- (:slides-count db) 1) 0]
                                                        :focus-slide (- (:slides-count db) 1)) ; если последний слайд
                                            (as-> (assoc db :current-slides (->> (nth (:current-slides db) 2)
                                                                                 inc
                                                                                 (conj (into [] (take-last 2 (:current-slides db)))))) updated-db
                                                  (assoc updated-db :focus-slide (second (:current-slides updated-db)))))
            ; пролистывают влево + карусель
            (and (= (:sw-mod db) :carousel)
                 (or (:left-next-slide db)
                     (and (= -1 (:direction db))
                          fast-swipe?))) (condp = (:focus-slide db)
                                            1 (assoc db :current-slides [(- (:slides-count db) 1) 0 1]
                                                        :focus-slide 0)                      ; на первый слайд
                                            (as-> (assoc db :current-slides (-> (nth (:current-slides db) 0)
                                                                                dec
                                                                                (cons (take 2 (:current-slides db)))
                                                                                (->> (into [])))) updated-db
                                                  (assoc updated-db :focus-slide (second (:current-slides updated-db)))))
            ; пролистывают вправо в режиме :line
            (or (:right-next-slide db)
                (and (= 1 (:direction db))
                     fast-swipe?)) (condp = (- (:slides-count db) (:focus-slide db))
                                                 (:slides-count db) (update db :focus-slide inc) ; если первый слайд
                                                 1 db                                            ; если последний слайд
                                                 (as-> (assoc db :current-slides (->> (nth (:current-slides db) 2)
                                                                                      inc
                                                                                      (conj (into [] (take-last 2 (:current-slides db)))))) updated-db
                                                       (assoc updated-db :focus-slide (second (:current-slides updated-db)))))
            ; пролистывают влево в режиме :line
            (or (:left-next-slide db)
                (and (= -1 (:direction db))
                     fast-swipe?)) (condp = (:focus-slide db)
                                                 (:slides-count db) (update db :focus-slide dec) ; если последний слайд
                                                 1 (update db :focus-slide dec)                  ; если второй слайд
                                                 0 db                                            ; если первый слайд
                                                 (as-> (assoc db :current-slides (-> (nth (:current-slides db) 0)
                                                                                     dec
                                                                                     (cons (take 2 (:current-slides db)))
                                                                                     (->> (into [])))) updated-db
                                                       (assoc updated-db :focus-slide (second (:current-slides updated-db)))))
            ; если выше случаи не подошли
            :else db)
        (assoc :start-touch-position 0
               :temporary-swipe 0
               :fast-swipe? fast-swipe?
               :duration true)))))

(re-frame/reg-event-db
  ::transition-end
  [add-swipe-component-key]
  (fn [db _]
    (assoc db :start-touch-position 0
              :temporary-swipe 0
              :right-next-slide false
              :left-next-slide false
              :direction nil
              :fast-swipe? false
              :duration false)))