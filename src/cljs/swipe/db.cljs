(ns swipe.db)

(def default-db
  {:swipe-component
   {:start-touch-position 0
    :main-wrapper-width 0
    :current-slides []
    :right-next-slide false
    :left-next-slide false
    :temporary-swipe 0
    :slides-count 0
    :focus-slide 1
    :start-swipe-time nil
    :direction nil
    :sw-mod :line
    :duration false
    :fast-swipe? false}})
