(ns swipe.subs
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
  ::swipe-component
  (fn [db]
    (:swipe-component db)))

(re-frame/reg-sub
  ::main-wrapper-width
  :<- [::swipe-component]
  (fn [swipe-component]
    (:main-wrapper-width swipe-component)))

(re-frame/reg-sub
  ::inner-block-width
  :<- [::main-wrapper-width]
  (fn [main-wrapper-width]
    (str (* 3 main-wrapper-width) "px")))

(re-frame/reg-sub
  ::current-slides
  :<- [::swipe-component]
  (fn [swipe-component]
    (:current-slides swipe-component)))

(re-frame/reg-sub
  ::temporary-swipe
  :<- [::swipe-component]
  (fn [swipe-component _]
    (:temporary-swipe swipe-component)))

(re-frame/reg-sub
  ::focus-slide
  :<- [::swipe-component]
  (fn [swipe-component _]
    (:focus-slide swipe-component)))

(re-frame/reg-sub
  ::slides-count
  :<- [::swipe-component]
  (fn [swipe-component _]
    (:slides-count swipe-component)))

(re-frame/reg-sub
  ::sw-mod
  :<- [::swipe-component]
  (fn [swipe-component _]
    (:sw-mod swipe-component)))

(re-frame/reg-sub
  ::end-animation
  :<- [::swipe-component]
  (fn [db _]
    (:duration db)))

(re-frame/reg-sub
  ::focus-slide-order
  :<- [::current-slides]
  :<- [::focus-slide]
  :<- [::slides-count]
  :<- [::sw-mod]
  (fn [[current-slides focus-slide slides-count sw-mod] _]
    (if (= sw-mod :carousel)
      "centre"
      (condp = focus-slide
        (first current-slides) "left"
        (- slides-count 1) "right"
        "centre"))))

(re-frame/reg-sub
  ::slides-vector
  :<- [::main-wrapper-width]
  :<- [::temporary-swipe]
  :<- [::slides-count]
  :<- [::focus-slide-order]
  :<- [::current-slides]
  :<- [::end-animation]
  (fn [[main-wrapper-width temporary-swipe slides-count focus-slide-order current-slides end-animation] _]
    (let [slide-styles (fn [margin? main-wrapper-width temporary-swipe]
                           (if margin?
                             {:style {:width main-wrapper-width
                                      :transform (str "translate(" temporary-swipe "px)")
                                      :margin-left (* -1 main-wrapper-width)}}
                             {:style {:width main-wrapper-width
                                      :transform (str "translate(" temporary-swipe "px)")}}))
          transition-map (fn [style-map end-animation]
                           (if end-animation
                               (assoc-in style-map [:style :transition] "transform 0.5s")
                               style-map))
          definite-index (fn [common-map i] (merge common-map
                                                   {:index i}))
          get-slide-index (fn [common-map n] (merge common-map
                                                    {:index (get current-slides n)}))
          assemble-slide-map (fn [{:keys [margin? definite-index? n main-wrapper-width temporary-swipe]}]
                               (if definite-index?
                                 (-> (slide-styles margin? main-wrapper-width temporary-swipe)
                                     (transition-map end-animation)
                                     (definite-index n))
                                 (-> (slide-styles margin? main-wrapper-width temporary-swipe)
                                     (transition-map end-animation)
                                     (get-slide-index n))))]
      (condp = slides-count
        1 [(assemble-slide-map {:margin? false
                                :definite-index? true
                                :n 0
                                :main-wrapper-width main-wrapper-width
                                :temporary-swipe temporary-swipe})]
        2 (condp = focus-slide-order
            "left" [(assemble-slide-map {:margin? false
                                         :definite-index? true
                                         :n 0
                                         :main-wrapper-width main-wrapper-width
                                         :temporary-swipe temporary-swipe})
                    (assemble-slide-map {:margin? false
                                         :definite-index? true
                                         :n 1
                                         :main-wrapper-width main-wrapper-width
                                         :temporary-swipe temporary-swipe})]
            "right" [(assemble-slide-map {:margin? true
                                          :definite-index? true
                                          :n 0
                                          :main-wrapper-width main-wrapper-width
                                          :temporary-swipe temporary-swipe})
                     (assemble-slide-map {:margin? false
                                          :definite-index? true
                                          :n 1
                                          :main-wrapper-width main-wrapper-width
                                          :temporary-swipe temporary-swipe})]
            ; если 2 слайда и режим :carousel (он при двух слайдах перелючает focus-slide-order в "center")
            [(assemble-slide-map {:margin? true
                                  :definite-index? false
                                  :n 0
                                  :main-wrapper-width main-wrapper-width
                                  :temporary-swipe temporary-swipe})
             (assemble-slide-map {:margin? false
                                  :definite-index? false
                                  :n 1
                                  :main-wrapper-width main-wrapper-width
                                  :temporary-swipe temporary-swipe})
             (assemble-slide-map {:margin? false
                                  :definite-index? false
                                  :n 2
                                  :main-wrapper-width main-wrapper-width
                                  :temporary-swipe temporary-swipe})])
        (condp = focus-slide-order ; текущий слайд первый или последний?
          "left" [(assemble-slide-map {:margin? false
                                       :definite-index? true
                                       :n 0 ; первый слайд
                                       :main-wrapper-width main-wrapper-width
                                       :temporary-swipe temporary-swipe})
                  (assemble-slide-map {:margin? false
                                       :definite-index? true
                                       :n 1 ; второй слайд
                                       :main-wrapper-width main-wrapper-width
                                       :temporary-swipe temporary-swipe})]
          "right" [(assemble-slide-map {:margin? true
                                        :definite-index? true
                                        :n (- slides-count 2) ; предпоследний слайд
                                        :main-wrapper-width main-wrapper-width
                                        :temporary-swipe temporary-swipe})
                   (assemble-slide-map {:margin? false
                                        :definite-index? true
                                        :n (- slides-count 1) ; последний слайд
                                        :main-wrapper-width main-wrapper-width
                                        :temporary-swipe temporary-swipe})]
          [(assemble-slide-map {:margin? true
                                :definite-index? false
                                :n 0
                                :main-wrapper-width main-wrapper-width
                                :temporary-swipe temporary-swipe})
           (assemble-slide-map {:margin? false
                                :definite-index? false
                                :n 1
                                :main-wrapper-width main-wrapper-width
                                :temporary-swipe temporary-swipe})
           (assemble-slide-map {:margin? false
                                :definite-index? false
                                :n 2
                                :main-wrapper-width main-wrapper-width
                                :temporary-swipe temporary-swipe})])))))