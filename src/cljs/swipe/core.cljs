(ns swipe.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [swipe.events :as events]
            [swipe.views :as views]
            [swipe.config :as config]))


(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (println "dev mode")))

(def start-slider-component views/slider-component-view)

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render (start-slider-component :shots [[:h1 "1111"]
                                                  [:h1 "2222"]
                                                  [:h1 "3333"]
                                                  [:h1 "4444"]
                                                  [:h1 "5555"]]
                                          :height "500px"
                                          :sw-mod :carousel)
                  (.getElementById js/document "app")))

(defn ^:export init []
  (re-frame/dispatch-sync [::events/initialize-db])
  (dev-setup)
  (mount-root))
