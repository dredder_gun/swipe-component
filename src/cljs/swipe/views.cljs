(ns swipe.views
  (:require [re-frame.core :as re-frame]
            [re-com.core :as re-com]
            [swipe.subs :as subs]
            [swipe.events :as events]
            [reagent.core :as reagent]))

(defn wrapper [slides sw-mod]
  (let [inner-block-width (re-frame/subscribe [::subs/inner-block-width])
        previous-slides (atom nil)
        previous-sw-mod (atom nil)
        slides-vector (re-frame/subscribe [::subs/slides-vector])]
    (reagent/create-class {:reagent-render (fn [slides sw-mod]
                                             (.log js/console "slides-vector" @slides-vector)
                                             (let [_ (when (not= @previous-slides slides)
                                                       (do
                                                         (re-frame/dispatch [::events/setup slides sw-mod])
                                                         (reset! previous-slides slides)))
                                                   _ (when (not= @previous-sw-mod sw-mod)
                                                       (do
                                                         (re-frame/dispatch [::events/setup slides sw-mod])
                                                         (reset! previous-sw-mod sw-mod)))]
                                               [:div {:class "main-wrapper"}
                                                [re-com/h-box
                                                 :class "inner-block"
                                                 :attr {:on-touch-start #(re-frame/dispatch [::events/start-coord (aget % "touches" 0 "pageX")])
                                                        :on-touch-move #(do
                                                                          (.preventDefault %)
                                                                          (re-frame/dispatch [::events/swipe-element (aget % "touches" 0 "pageX")]))
                                                        :on-touch-end #(re-frame/dispatch [::events/end-move])
                                                        :on-transition-end #(do
                                                                              (.log js/console "on-transition-end")
                                                                              (re-frame/dispatch [::events/transition-end]))}
                                                 :style {:width @inner-block-width}
                                                 :children (for [slide @slides-vector]
                                                             [:div.shot {:style (:style slide)}
                                                              (get slides (:index slide))])]]))
                           :component-did-mount (fn [this]
                                                  (re-frame/dispatch [::events/setup slides sw-mod (.-offsetWidth (reagent/dom-node this))]))})))

(defn slider-component-view [& {:keys [shots height sw-mod]
                                :or   {shots []
                                       height "100%"
                                       sw-mod :line}}]
  (if (and (string? height)
           (vector? shots)
           (or (= :line sw-mod)
               (= :carousel sw-mod)))
    [re-com/v-box
     :height height
     :children [[wrapper shots sw-mod]]]
    (do
      (.error js/console "height должен быть в формате string, shots - vector, sw-mod имеет занчение либо :line либо :carousel")
      [:h1 {:styles {:color "red"}} "Произошла ошибка при создании свайп-компонента"])))
