(ns swipe.test-events
  (:require [re-frame.core :as re-frame]
            [swipe.events :as events]))

(re-frame/reg-event-db
  ::initialize-test-db
  (fn  [_ _]
    {:swipe-component
     {:start-touch-position 0
      :main-wrapper-width 0
      :current-slides []
      :right-next-slide false
      :left-next-slide false
      :temporary-swipe 0
      :slides-count 0
      :focus-slide 1
      :start-swipe-time nil
      :direction nil
      :sw-mod :line}}))

(re-frame/reg-event-db
  ::test-swipe
  [events/add-swipe-component-key re-frame/trim-v]
  (fn  [db [direction]]
    (case direction
      "right" (assoc db :right-next-slide true)
      "left" (assoc db :left-next-slide true))))