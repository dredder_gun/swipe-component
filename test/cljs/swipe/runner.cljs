(ns swipe.runner
    (:require [doo.runner :refer-macros [doo-tests]]
              [swipe.carousel-test]
              [swipe.line-test]))

; todo сделать тесты для 1-го слайда и для двух

(doo-tests 'swipe.line-test
           'swipe.carousel-test)
