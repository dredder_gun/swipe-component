(ns swipe.line-test
  (:require [cljs.test :refer-macros [deftest testing is]]
            [swipe.events :as events]
            [swipe.subs :as subs]
            [day8.re-frame.test :as rf-test]
            [re-frame.core :as re-frame]
            [swipe.test-events :as test-events]))

(deftest test-slides
  (rf-test/run-test-sync
    (re-frame/dispatch [::test-events/initialize-test-db])
    (re-frame/dispatch [::events/setup [[:p "1111"]
                                        [:p "2222"]
                                        [:p "3333"]
                                        [:p "4444"]
                                        [:p "5555"]]
                                       :line
                                       900])
    (let [focus-slide (re-frame/subscribe [::subs/focus-slide])
          current-slides (re-frame/subscribe [::subs/current-slides])]
      (is (= @focus-slide 0))
      (is (= @current-slides [0 1 2]))
      (re-frame/dispatch [::test-events/test-swipe "right"])
      (re-frame/dispatch [::events/end-move])
      (re-frame/dispatch [::events/transition-end])
      (is (= @focus-slide 1))
      (is (= @current-slides [0 1 2]))
      (re-frame/dispatch [::test-events/test-swipe "right"])
      (re-frame/dispatch [::events/end-move])
      (re-frame/dispatch [::events/transition-end])
      (is (= @focus-slide 2))
      (is (= @current-slides [1 2 3]))
      (re-frame/dispatch [::test-events/test-swipe "right"])
      (re-frame/dispatch [::events/end-move])
      (re-frame/dispatch [::events/transition-end])
      (is (= @focus-slide 3))
      (is (= @current-slides [2 3 4]))
      (re-frame/dispatch [::test-events/test-swipe "right"])
      (re-frame/dispatch [::events/end-move])
      (re-frame/dispatch [::events/transition-end])
      (is (= @focus-slide 4))
      (is (= @current-slides [3 4 5]))
      (re-frame/dispatch [::test-events/test-swipe "right"])
      (re-frame/dispatch [::events/end-move])
      (re-frame/dispatch [::events/transition-end])
      (is (= @focus-slide 4))
      (is (= @current-slides [3 4 5]))
      (re-frame/dispatch [::test-events/test-swipe "left"])
      (re-frame/dispatch [::events/end-move])
      (re-frame/dispatch [::events/transition-end])
      (is (= @focus-slide 3))
      (is (= @current-slides [2 3 4]))
      (re-frame/dispatch [::test-events/test-swipe "left"])
      (re-frame/dispatch [::events/end-move])
      (re-frame/dispatch [::events/transition-end])
      (is (= @focus-slide 2))
      (is (= @current-slides [1 2 3]))
      (re-frame/dispatch [::test-events/test-swipe "left"])
      (re-frame/dispatch [::events/end-move])
      (re-frame/dispatch [::events/transition-end])
      (is (= @focus-slide 1))
      (is (= @current-slides [0 1 2]))
      (re-frame/dispatch [::test-events/test-swipe "left"])
      (re-frame/dispatch [::events/end-move])
      (re-frame/dispatch [::events/transition-end])
      (is (= @focus-slide 0))
      (is (= @current-slides [0 1 2])))))